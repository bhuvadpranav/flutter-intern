
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'package:google_fonts/google_fonts.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  double? appBarHeight;
  bool appBarSearchShow=false;
  bool isSwitch=false;

  final TextEditingController searchController=TextEditingController();

  List<String> name=["Yashika","Yashika","Yashika","Yashika","Yashika","Yashika","Yashika","Yashika","Yashika","Yashika"];
  List<int> age=[29,29,29,29,29,29,29,29,29,29];
  List<String> image=["assets/Images/profile.png","assets/Images/profile.png","assets/Images/profile.png","assets/Images/profile.png","assets/Images/profile.png","assets/Images/profile.png","assets/Images/profile.png","assets/Images/profile.png","assets/Images/profile.png","assets/Images/profile.png"];
  List<bool> follow=[false,false,false,false,false,false,false,false,false,false];
  

  Future<void> showBottomSheet(){
    return showModalBottomSheet(
    
      context: context, 
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(15),
          topRight: Radius.circular(15),
        )
      ),
      builder: (context){
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.only(top:32,left:20),
              child: Row(
                children: [
                  SizedBox(
                    height: 24,
                    width: 24,
                    child:  Image.asset("assets/Images/link.png"),
                  ),
                  const SizedBox(width: 10,),
                  Text("Invite",style: GoogleFonts.quicksand(fontSize:16,fontWeight:FontWeight.w600,))
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top:24,left:20),
              child: Row(
                children: [
                  SizedBox(
                    height: 24,
                    width: 24,
                    child:  Image.asset("assets/Images/user-add.png"),
                  ),
                  
                  const SizedBox(width: 10,),
                  Text("Add member",style: GoogleFonts.quicksand(fontSize:16,fontWeight:FontWeight.w600,))
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top:24,left:20,bottom: 20),
              child: Row(
                children: [
                  SizedBox(
                    height: 24,
                    width: 24,
                    child:  Image.asset("assets/Images/user-add group.png"),
                  ),
                  
                  const SizedBox(width: 10,),
                  Text("Add Group",style: GoogleFonts.quicksand(fontSize:16,fontWeight:FontWeight.w600,))
                ],
              ),
            )
          ],
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    appBarHeight = MediaQuery.of(context).padding.top + kToolbarHeight;
    return  Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverLayoutBuilder(
            builder: (BuildContext context,constraints){
            final scrolled=constraints.scrollOffset > 0;
            return SliverAppBar(
              bottom: (!scrolled)?PreferredSize(
                preferredSize: const Size.fromHeight(78),
                child:Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left:20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(height:10),
                          Text("The weeknd",style: GoogleFonts.quicksand(fontSize:22,fontWeight:FontWeight.w600,color:const Color.fromRGBO(255, 255, 255, 1))),
                          const SizedBox(height: 5,),
                          Text("Community • +11K Members",style: GoogleFonts.quicksand(fontSize:12,fontWeight:FontWeight.w600,color:const Color.fromRGBO(255, 255, 255, 1)))
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right:20),
                      child: GestureDetector(
                        onTap: (){},
                        child: Image.asset("assets/Images/Frame 2526.png"),
                      ),
                    )
                  ],
                ),
              ):PreferredSize(
                preferredSize: const Size.fromHeight(100),
                child:Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left:20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const CircleAvatar(
                            radius: 27,

                            backgroundImage: AssetImage("assets/Images/image appbar.jpg"),
                          ),
                          const SizedBox( width: 10,),
                          Column(
                            children: [
                              const SizedBox(height:10),
                              Text("The weeknd",style: GoogleFonts.quicksand(fontSize:22,fontWeight:FontWeight.w600,color:const Color.fromRGBO(255, 255, 255, 1))),
                              const SizedBox(height: 5,),
                              Text("Community • +11K Members",style: GoogleFonts.quicksand(fontSize:12,fontWeight:FontWeight.w600,color:const Color.fromRGBO(255, 255, 255, 1))),
                            ],
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right:20),
                      child: Container(
                        height: 24,
                        width: 24,
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle
                        ),
                        child: GestureDetector(
                          onTap: (){
                            showBottomSheet();
                          },
                          child: Image.asset("assets/Images/three dots.png"),
                        ),
                      ),
                    )
                  ],
                ),
               ),
              backgroundColor: const Color.fromRGBO(195, 36, 34, 1),
              elevation: 0,
              pinned: true,
              toolbarHeight: 0.0,
              expandedHeight: 300,
              flexibleSpace: (!scrolled)?const FlexibleSpaceBar(
                background: Image(
                  image: AssetImage("assets/Images/image appbar.jpg"),
                  fit: BoxFit.fitWidth,
                  width: double.maxFinite,
                  height: 185,
                ),
              ):null,
              
            );
            }
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.only(top:20.0,left:20),
              child: Column(
                children: [
                  SizedBox(
                    height: 114,
                    child: Row(
                      children: [
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(right:10),
                            child: Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod vestibulum lacus, nec consequat nulla efficitur sit amet. Proin eu lorem libero. Sed id enim in urna tincidunt sodales. Vivamus vel semper ame...Read more",
                              style: GoogleFonts.quicksand(
                                fontSize:16,
                                fontWeight:FontWeight.w400,
                              ),
                            ),
                          ),
                        ),
                        // Text(
                        //   "...Read more",
                        //   style: GoogleFonts.quicksand(fontSize:16,fontWeight:FontWeight.w400,color:Colors.pink,
                        // )
                        // ),
                      ],
                    )
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top:16,right: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          height: 20,
                          width: 70,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            border: Border.all(color: const Color.fromRGBO(239, 69, 111, 1),)
                          ),
                          child:Text("Outdoor",style: GoogleFonts.quicksand(fontSize:12,fontWeight:FontWeight.w400,color: const Color.fromRGBO(239, 69, 111, 1)),)
                        ),
                        Container(
                          height: 20,
                          width: 70,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            border: Border.all(color: const Color.fromRGBO(239, 69, 111, 1),)
                          ),
                          child:Text("Outdoor",style: GoogleFonts.quicksand(fontSize:12,fontWeight:FontWeight.w400,color: const Color.fromRGBO(239, 69, 111, 1)),)
                        ),
                        Container(
                          height: 20,
                          width: 70,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            border: Border.all(color: const Color.fromRGBO(239, 69, 111, 1),)
                          ),
                          child:Text("Outdoor",style: GoogleFonts.quicksand(fontSize:12,fontWeight:FontWeight.w400,color: const Color.fromRGBO(239, 69, 111, 1)),)
                        ),
                        Container(
                          height: 20,
                          width: 70,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            border: Border.all(color: const Color.fromRGBO(239, 69, 111, 1),)
                          ),
                          child:Text("Outdoor",style: GoogleFonts.quicksand(fontSize:12,fontWeight:FontWeight.w400,color: const Color.fromRGBO(239, 69, 111, 1)),)
                        ),
                        Container(
                          height: 20,
                          width: 31,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            border: Border.all(color: const Color.fromRGBO(239, 69, 111, 1),)
                          ),
                          child:Text("+1",style: GoogleFonts.quicksand(fontSize:12,fontWeight:FontWeight.w400,color: const Color.fromRGBO(239, 69, 111, 1)),)
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top:36),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Media, docs and links",style:GoogleFonts.quicksand(fontSize:18,fontWeight:FontWeight.w600)),
                            const Padding(
                              padding: EdgeInsets.only(right:20),
                              child: Icon(Icons.arrow_forward_ios),
                            )
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Padding(
                      padding: const EdgeInsets.only(top:12,left: 10),
                      child: Row(
                      children: [
                       Padding(
                        padding: const EdgeInsets.only(left:10,right:5,),
                          child: Container(
                            height: 100,
                            width: 100,
                            decoration: BoxDecoration(
                               borderRadius: BorderRadius.circular(5),
                            ),
                            child: Image.asset("assets/Images/Frame 2nd.jpg"),
                         ),
                       ),
                       Padding(
                        padding: const EdgeInsets.only(right:5),
                          child: Container(
                            height: 100,
                            width: 100,
                            decoration: BoxDecoration(
                               borderRadius: BorderRadius.circular(5),
                            ),
                            child: Image.asset("assets/Images/Frame 2nd.jpg"),
                         ),
                       ),
                       Padding(
                        padding: const EdgeInsets.only(right:5),
                          child: Container(
                            height: 100,
                            width: 100,
                            decoration: BoxDecoration(
                               borderRadius: BorderRadius.circular(5),
                            ),
                            child: Image.asset("assets/Images/Frame 2nd.jpg"),
                         ),
                       ),   
                       Padding(
                        padding: const EdgeInsets.only(right:5),
                          child: Container(
                            height: 100,
                            width: 100,
                            decoration: BoxDecoration(
                               borderRadius: BorderRadius.circular(5),
                            ),
                            child: Image.asset("assets/Images/Frame 2nd.jpg"),
                         ),
                       ),Padding(
                        padding: const EdgeInsets.only(right:5),
                          child: Container(
                            height: 100,
                            width: 100,
                            decoration: BoxDecoration(
                               borderRadius: BorderRadius.circular(5),
                            ),
                            child: Image.asset("assets/Images/Frame 2nd.jpg"),
                         ),
                       ),             
                      ],
                     ),
                    ),
                ),
              ]
            )
          ),
          SliverToBoxAdapter(
            child:Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left:20,top:16),
                  child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Mute notification",style:GoogleFonts.quicksand(fontSize:14,fontWeight:FontWeight.w600)),
                    Padding(
                      padding:const  EdgeInsets.only(right:20),
                      child: Container(
                        height: 25,
                        width: 47,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20)
                        ),
                        child: Switch(
                          value:isSwitch , 
                          onChanged: (value){
                            setState(() {
                              isSwitch=value;
                            });
                          }),
                      ),
                     )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left:20,top:21),
                  child: Row(
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(right:10),
                      child: Icon(Icons.delete),
                    ),
                    Text("Clear chat",style:GoogleFonts.quicksand(fontSize:14,fontWeight:FontWeight.w600)),
                    
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left:20,top:21),
                  child: Row(
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(right:10),
                      child: Icon(Icons.lock_outline_rounded),
                    ),
                    Text("Encryption",style:GoogleFonts.quicksand(fontSize:14,fontWeight:FontWeight.w600)),
                    
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left:20,top:21),
                  child: Row(
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(right:10),
                      child: Icon(Icons.logout),
                    ),
                    Text("Exit community",style:GoogleFonts.quicksand(fontSize:14,fontWeight:FontWeight.w600)),
                    
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left:20,top:21),
                  child: Row(
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(right:10),
                      child: Icon(Icons.thumb_down),
                    ),
                    Text("Report",style:GoogleFonts.quicksand(fontSize:14,fontWeight:FontWeight.w600)),  
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left:20,top:21),
                  child: (appBarSearchShow)
                  ?Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Members",style:GoogleFonts.quicksand(fontSize:16,fontWeight:FontWeight.w600)),
                    Padding(
                      padding:const EdgeInsets.only(right:10),
                      child: GestureDetector(
                        onTap: (){
                          appBarSearchShow=true;
                          setState(() {
                            
                          });
                        },
                        child: const Icon(Icons.search)
                      ),
                    ),
                    ],
                  ):Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                        Expanded(
                          child: SizedBox(
                            height: 32,
                            child: TextFormField(
                              controller: searchController,
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                fillColor: const Color.fromRGBO(233, 233, 235, 1),
                                hintText: 'Search Members',
                                hintStyle: GoogleFonts.quicksand(
                                  fontSize:14,
                                  fontWeight:FontWeight.w400,
                                  
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20)
                                )
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(width: 10,),
                        Padding(
                          padding: const EdgeInsets.only(right:10.0),
                          child: GestureDetector(
                            onTap: (){
                              appBarSearchShow=false;
                            },
                            child: Container(
                              height: 20,
                              width: 50,
                              alignment: Alignment.center ,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10)
                              ),
                              child: Text("Cancel",style: GoogleFonts.quicksand(fontSize:14,fontWeight:FontWeight.w600),),
                            ),
                          ),
                        )

                    ],
                  ),
                ),
              ],
            ) ,
          ),
           SliverList(
            delegate: SliverChildBuilderDelegate(
                (BuildContext context,int index){
                  return Padding(
                    padding: const EdgeInsets.only(top:16,left:20,right:20),
                    child: Row(
                      children: [
                        SizedBox(
                          height: 41,
                          width: 41,
                          child: Image.asset(image[index]),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Column(
                          children: [
                            Text(name[index],style:GoogleFonts.quicksand(fontSize:14,fontWeight:FontWeight.w600)),
                            
                            Row(
                              children: [
                                Text("29, ",style:GoogleFonts.quicksand(fontSize:12,fontWeight:FontWeight.w600,color:const Color.fromRGBO(73, 73, 73, 1))),
                                Text("india",style:GoogleFonts.quicksand(fontSize:12,fontWeight:FontWeight.w600,color:const Color.fromRGBO(73, 73, 73, 1))),
                              ],
                            ),
                          ],
                        ),
                        const Spacer(),
                        Container(
                          height: 32,
                          width: 101,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: (follow[index])?const Color.fromRGBO(170, 171, 171, 1):const Color.fromRGBO(239, 69, 111, 1)
                          ),
                          child: GestureDetector(
                            onTap: (){
                             
                              setState(() {
                                follow[index]=!follow[index];
                              });
                            },
                            child: (follow[index])?Text("Following",style:GoogleFonts.quicksand(fontSize:16,fontWeight:FontWeight.w600,color:const Color.fromRGBO(255, 255, 255, 1))):Text("Add",style:GoogleFonts.quicksand(fontSize:16,fontWeight:FontWeight.w600,color:const Color.fromRGBO(255, 255, 255, 1))),
                          ),
                          
                        )
                      ],
                    ),
                  );
                } ,
                childCount: 10
            )
          )
        ],
      ),
    );
  }
}

// SliverAppBar(
//                             backgroundColor: Colors.white,
//                             expandedHeight:300,
//                             floating: false,
//                             pinned:true,
//                             flexibleSpace: LayoutBuilder(
//                               builder: (BuildContext context,BoxConstraints constraints){
//                                 if(constraints.biggest.height==appBarHeight){
//                                   appBarSearchShow=true;
//                                 }
//                                 else{
//                                   appBarSearchShow=false;
//                                 }
//                                 return FlexibleSpaceBar(
//                                   collapseMode: CollapseMode.parallax,
//                                   titlePadding: const EdgeInsets.only(bottom: 10),
//                                   centerTitle: true,
//                                   title: 
//                                     Container(
//                                       constraints: const BoxConstraints(minHeight: 30,maxHeight: 30),
//                                       width: 220,
//                                       decoration: BoxDecoration(
//                                         boxShadow: [
//                                           BoxShadow(
//                                             color: Colors.grey.withOpacity(0.6),
//                                             offset: const Offset(1.1,1.1),
//                                             blurRadius: 5.0
//                                           )
//                                         ]
//                                       ),
//                                       child: Column(
//                                         children: [
//                                           Row(
//                                             children: [
//                                               TextField(
//                                                 controller: searchController,
//                                                 keyboardType: TextInputType.text,
//                                                 decoration: InputDecoration(
//                                                   hintText: 'Search',
//                                                   hintStyle: GoogleFonts.quicksand(
//                                                     fontSize:14,
//                                                     fontWeight:FontWeight.w400
//                                                   ),
//                                                   fillColor: Colors.white,
//                                                   border: OutlineInputBorder(
//                                                     borderRadius: BorderRadius.circular(8)
                                                  
//                                                   )
//                                                 ),
//                                               ),
//                                               ElevatedButton(
//                                                 onPressed:(){
//                                                   Navigator.of(context).pop();
//                                                 }, 
//                                                 child: Text("Cancel")
//                                                )
//                                             ],
//                                           ),
//                                           ListView.builder(
//                                             itemCount: name.length,
//                                             itemBuilder: (context,index){
//                                               return Padding(
//                                                 padding: const EdgeInsets.only(top:16,left:20,right:20),
//                                                 child: Row(
//                                                   children: [
//                                                     SizedBox(
//                                                       height: 41,
//                                                       width: 41,
//                                                       child: Image.asset(image[index]),
//                                                     ),
//                                                     const SizedBox(
//                                                       width: 10,
//                                                     ),
//                                                     Column(
//                                                       children: [
//                                                         Text(name[index],style:GoogleFonts.quicksand(fontSize:14,fontWeight:FontWeight.w600)),
                                                        
//                                                         Row(
//                                                           children: [
//                                                             Text("29, ",style:GoogleFonts.quicksand(fontSize:12,fontWeight:FontWeight.w600,color:const Color.fromRGBO(73, 73, 73, 1))),
//                                                             Text("india",style:GoogleFonts.quicksand(fontSize:12,fontWeight:FontWeight.w600,color:const Color.fromRGBO(73, 73, 73, 1))),
//                                                           ],
//                                                         ),
//                                                       ],
//                                                     ),
//                                                     const Spacer(),
//                                                     Container(
//                                                       height: 32,
//                                                       width: 101,
//                                                       alignment: Alignment.center,
//                                                       decoration: BoxDecoration(
//                                                         borderRadius: BorderRadius.circular(20),
//                                                         color: (follow[index])?const Color.fromRGBO(170, 171, 171, 1):const Color.fromRGBO(239, 69, 111, 1)
//                                                       ),
//                                                       child: GestureDetector(
//                                                         onTap: (){
                                                        
//                                                           setState(() {
//                                                             follow[index]=!follow[index];
//                                                           });
//                                                         },
//                                                         child: (follow[index])?Text("Following",style:GoogleFonts.quicksand(fontSize:16,fontWeight:FontWeight.w600,color:const Color.fromRGBO(255, 255, 255, 1))):Text("Add",style:GoogleFonts.quicksand(fontSize:16,fontWeight:FontWeight.w600,color:const Color.fromRGBO(255, 255, 255, 1))),
//                                                       ),
                                                      
//                                                     )
//                                                   ],
//                                                 ),
//                                               );
//                                             },
//                                           )
//                                         ],
//                                       ),
//                                     ),
//                                 );
//                               },
//                               ),
//                           );